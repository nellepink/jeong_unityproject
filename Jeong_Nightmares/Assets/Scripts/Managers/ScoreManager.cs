﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;



	Text score_text;
	int highScore;
	public Text hs_text;


    void Awake ()
    {
		// Set up the reference.
		score_text = GetComponent <Text> ();

		highScore = PlayerPrefs.GetInt("highScore", 0);
		print ("highScore: " + highScore);

    }


    void Update ()
    {
		score_text.text = "Score: " + score;

		if (score > highScore) 
		{
			highScore = score;
			PlayerPrefs.SetInt ("highScore", highScore);

		}
		hs_text.text = "highScore: " + highScore;
    }
}
